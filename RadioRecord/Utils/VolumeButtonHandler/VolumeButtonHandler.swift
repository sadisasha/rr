//
//  VolumeButtonHandler.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 17.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import MediaPlayer

let kSessionVolumeKeyPath = "outputVolume"

class VolumeButtonHandler: NSObject {

    private let maxVolume: CGFloat = 0.99999
    private let minVolume: CGFloat = 0.00001
    
    private var appIsActive = false
    
    private var session: AVAudioSession?
    private var volumeView: MPVolumeView?
    private var initialVolume: CGFloat = 0
    
    private var sessionContext: UnsafeMutablePointer<Void>?
    
    private var upBlock: (() -> Void)?
    private var downBlock: (() -> Void)?
    
    //MARK: - Class initialization
    
    override init() {
        super.init()
        
        self.appIsActive = true
        
        self.setupSession()
        self.disableVolumeHUD()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(applicationDidChangeActive(_:)), name: UIApplicationWillResignActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(applicationDidChangeActive(_:)), name: UIApplicationDidBecomeActiveNotification, object: nil)
        
        let delayInSeconds = 0.1
        let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delayInSeconds * Double(NSEC_PER_SEC)))
        dispatch_after(popTime, dispatch_get_main_queue()) { () -> Void in
            
            self.setInitialVolume()
        }
    }
    
    deinit {
        
        self.session?.removeObserver(self, forKeyPath: kSessionVolumeKeyPath)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.volumeView?.removeFromSuperview()
    }
    
    //MARK: - Public methods
    
    class func volumeButtonHandlerWithUpBlock(upBlock: (() -> Void)?, downBlock: (() -> Void)?) -> VolumeButtonHandler? {
        
        let instance = VolumeButtonHandler()

        instance.upBlock = upBlock
        instance.downBlock = downBlock
        
        return instance
    }
    
    //MARK: - Private methods
    
    private func setupSession() {
        
        self.session = AVAudioSession.sharedInstance()
        
        do {
            
            try self.session?.setCategory(AVAudioSessionCategoryAmbient, withOptions: AVAudioSessionCategoryOptions.DefaultToSpeaker)
            try self.session?.setActive(true)
            
        } catch {
            
            print(error)
            return
        }

        self.session?.addObserver(self, forKeyPath: kSessionVolumeKeyPath, options: [.Old, .New], context: &sessionContext)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(audioSessionInterrupted(_:)), name: AVAudioSessionInterruptionNotification, object: nil)
    }
    
    private func setInitialVolume() {
        
        if let volume = self.session?.outputVolume {
         
            self.initialVolume = CGFloat(volume)
            
            if self.initialVolume > maxVolume {
                
                self.initialVolume = maxVolume
                self.setSystemVolume(self.initialVolume)
                
            } else if self.initialVolume < minVolume {
                
                self.initialVolume = minVolume
                self.setSystemVolume(self.initialVolume)
            }
        }
    }
    
    private func disableVolumeHUD() {
        
        self.volumeView = MPVolumeView(frame: CGRectMake(CGFloat.max, CGFloat.max, 0, 0))
        UIApplication.sharedApplication().windows.first?.addSubview(self.volumeView!)
    }
    
    private func setSystemVolume(volume: CGFloat) {
    
        MPMusicPlayerController.applicationMusicPlayer().setValue(volume, forKey: "volume")
    }
    
    //MARK: - NSNotificationCenter handle
    
    func applicationDidChangeActive(notification: NSNotification?) {
        
        self.appIsActive = (notification?.name == UIApplicationDidBecomeActiveNotification)
    }
    
    func audioSessionInterrupted(notification: NSNotification?) {
        
        if let interuptionDictionary = notification?.userInfo, interuptionType = interuptionDictionary[AVAudioSessionInterruptionTypeKey] as? UInt {
            
            if AVAudioSessionInterruptionType(rawValue: interuptionType) == AVAudioSessionInterruptionType.Ended {
                
                do {
                    
                    try self.session?.setActive(true)
                    
                } catch {
                    
                    print(error)
                }
            }
        }
    }
    
    //MARK: - KVO
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if self.sessionContext == context {
            
            if !self.appIsActive { return }
            
            let newVolume = change?[NSKeyValueChangeNewKey] as? CGFloat
            let oldVolume = change?[NSKeyValueChangeOldKey] as? CGFloat
            
            if self.initialVolume == newVolume { return }
            
            if newVolume > oldVolume {
              
                self.upBlock?()
                
            } else {
                
                self.downBlock?()
            }
            
            self.setSystemVolume(self.initialVolume)
            
        } else {
            
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
}
