//
//  FreeStreamer.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 30.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

protocol FreeStreamerDelegate {
    
    func freeStreamer(freeStreamer: FSAudioController?, onStateChange: FSAudioStreamState?)
    func freeStreamer(freeStreamer: FSAudioController?, onFailure: FSAudioStreamError?, description: String?)
    func freeStreamer(freeStreamer: FSAudioController?, onMetaDataAvailable: [NSObject: AnyObject]?)
}

class FreeStreamer: FSAudioController {

    var delegate: FreeStreamerDelegate?
    
    var currentStationName: String?
    var currentURL: NSURL?
    
    var paused = false
    
    var outputVolume: Float? {
        
        get {
            
            if let volume = NSUserDefaults.standardUserDefaults().objectForKey(kPlayerVolumeKey) as? Float {
                
                return volume
            }
            
            return 0.5
            
        } set {
            
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: kPlayerVolumeKey)
        }
    }
    
    private var lastPlaybackURL: NSURL?
    
    class var sharedInstance: FreeStreamer {
        
        struct Singleton {
            
            static let instance = FreeStreamer()
        }
        
        return Singleton.instance
    }
    
    func prepareToPlay() {
        
        self.setVolume(self.outputVolume!)
        
        self.stream.onStateChange = { (state) in
            
            self.delegate?.freeStreamer(self, onStateChange: state)
        }
        
        self.stream.onFailure = { (error, description) in
            
            self.delegate?.freeStreamer(self, onFailure: error, description: description)
        }
        
        self.stream.onMetaDataAvailable = { (metaData) in

            self.delegate?.freeStreamer(self, onMetaDataAvailable: metaData)
        }
    }
    
    override func play() {
        
        if self.paused {
            
            self.pause()
            self.paused = false
            
            return
        }
        
        if let currentURL = self.currentURL {
         
            if let lastPlaybackURL = self.lastPlaybackURL {
                
                if !lastPlaybackURL.isEqual(self.currentURL) {
                    
                    self.url = currentURL
                    super.play()
                
                } else if !self.isPlaying() {
                    
                    self.url = currentURL
                    super.play()
                }
                
            } else {
                
                self.url = currentURL
                super.play()
            }
            
            self.lastPlaybackURL = currentURL
        }
    }
    
    override func pause() {
        
        self.paused = true
        
        super.pause()
    }
    
    override func stop() {
        
        self.lastPlaybackURL = nil
        
        super.stop()
    }
    
    override func setVolume(volume: Float) {
        
        self.outputVolume = volume
        
        super.setVolume(volume)
    }
}
