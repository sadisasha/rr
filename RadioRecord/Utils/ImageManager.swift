//
//  ImageManager.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 26.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation
import UIKit

enum ExtType: String {
    
    case PNG = "png", JPG = "jpg"
}

class ImageManager {
    
    private class func imageNameFromTrack(track: Track) -> String {
        
        let name = "\(track.artist.stringByReplacingOccurrencesOfString(" ", withString: ""))-\(track.title.stringByReplacingOccurrencesOfString(" ", withString: ""))"
        
        return name.stringByReplacingOccurrencesOfString("'", withString: "")
    }
    
    private class func pathFromTrack(track: Track, type: ExtType) -> String? {
        
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsPath = NSString(string: paths[0])
        
        let folderPath = documentsPath.stringByAppendingPathComponent("RadioRecord")
        if !NSFileManager.defaultManager().fileExistsAtPath(folderPath) {
            
            do {
                
                try NSFileManager.defaultManager().createDirectoryAtPath(folderPath, withIntermediateDirectories: true, attributes: nil)
                
            } catch {
                
                RadioRecordLogger.Log(error)
                return nil
            }
        }
        
        let name = self.imageNameFromTrack(track)
        
        return (folderPath as NSString).stringByAppendingPathComponent("\(name).\(type.rawValue)")
    }
    
    class func save(track: Track, type: ExtType) {
        
        if let image = track.artworkImage {
            
            let data = UIImagePNGRepresentation(image)
            let path: String?
            
            switch type {
                
            case .PNG:
                path = self.pathFromTrack(track, type: ExtType.PNG)
                
            case .JPG:
                path = self.pathFromTrack(track, type: ExtType.JPG)
            }
            
            if let imageData = data, imagePath = path {
                
                imageData.writeToFile(imagePath, atomically: true)
            }
        }
    }
    
    class func read(track: Track) -> Track? {
        
        let name = self.imageNameFromTrack(track)

        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsPath = NSString(string: paths[0])
         
        let folderPath = documentsPath.stringByAppendingPathComponent("RadioRecord")
        
        if let elements = NSFileManager.defaultManager().enumeratorAtPath(folderPath) {

            while let element = elements.nextObject() {

                if (element as? String)?.rangeOfString(name) != nil {
                    
                    var tempPath: String?
                    
                    if element.hasSuffix("png") {
                        
                        tempPath = self.pathFromTrack(track, type: ExtType.PNG)
                    
                    } else {
                        
                        tempPath = self.pathFromTrack(track, type: ExtType.JPG)
                    }
                    
                    if let path = tempPath, imageData = NSData(contentsOfFile: path) {
                        
                        track.artworkImage = UIImage(data: imageData)
                        track.artworkLoaded = true
                        
                        return track
                    }
                }
            }
        }
        
        return nil
    }
    
    class func removeAll() {
        
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsPath = NSString(string: paths[0])
        
        let folderPath = documentsPath.stringByAppendingPathComponent("RadioRecord")
        if NSFileManager.defaultManager().fileExistsAtPath(folderPath) {
            
            do {
                
                try NSFileManager.defaultManager().removeItemAtPath(folderPath)
                
            } catch {
                
                RadioRecordLogger.Log(error)
            }
        }
    }
}