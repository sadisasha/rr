//
//  LastFmManager.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 16.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit
import MediaPlayer
import SwiftyJSON

enum ParserType: String {
    
    case Amazon = "Amazon", LastFm = "LastFm", iTunes = "iTunes", Local = "Local"
}

class MetadataManager {

    //*****************************************************************
    // Get LastFM/iTunes Data
    //*****************************************************************
    
    class func getTrackDataWithSuccess(queryURL: String, success: ((metaData: NSData!) -> Void)) {
        
        self.loadDataFromURL(NSURL(string: queryURL)!) { (data, error) -> Void in
            
            if let error = error {
                
                RadioRecordLogger.Log("API TIMEOUT OR ERROR")
                RadioRecordLogger.Log(error.localizedDescription)
            
            } else if let urlData = data {
                
                success(metaData: urlData)
            }
        }
    }

    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfig.allowsCellularAccess          = true
        sessionConfig.timeoutIntervalForRequest     = 15
        sessionConfig.timeoutIntervalForResource    = 30
        sessionConfig.HTTPMaximumConnectionsPerHost = 1
        
        let session = NSURLSession(configuration: sessionConfig)

        let loadDataTask = session.dataTaskWithURL(url){ data, response, error in
            if let responseError = error {
                completion(data: nil, error: responseError)
                
                RadioRecordLogger.Log("API ERROR: \(error?.localizedDescription)")

            } else if let httpResponse = response as? NSHTTPURLResponse {
                
                if httpResponse.statusCode != 200 {
                    
                    let statusError = NSError(domain:"ru.sav", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    
                    RadioRecordLogger.Log("API: HTTP status code has unexpected value")
                    
                    completion(data: nil, error: statusError)
                    
                } else {

                    completion(data: data, error: nil)
                }
            }
        }
        
        loadDataTask.resume()
    }

    class func metadataParser(station: Station?, track: Track?, metadata: [NSObject : AnyObject]?, process: () -> Void, success: (track: Track, finished: Bool) -> Void) {
        
        if let station = station, let track = track {
            
            if let timedMetadata = metadata where timedMetadata.count > 0 {
                
                let currentSongName = track.title
                
                if let artist = timedMetadata["MPMediaItemPropertyArtist"] as? String, title = timedMetadata["MPMediaItemPropertyTitle"] as? String {
                 
                    track.artist = artist
                    track.title = title
                    
                } else if var streamTitle = timedMetadata["StreamTitle"] as? String {

                    streamTitle = streamTitle.stringByReplacingOccurrencesOfString("(", withString: "").stringByReplacingOccurrencesOfString(")", withString: "")
                    
                    var stringParts = [String]()
                    if streamTitle.rangeOfString(" - ") != nil {
                        
                        stringParts = streamTitle.componentsSeparatedByString(" - ")
                        
                    } else {
                        
                        stringParts = streamTitle.componentsSeparatedByString("-")
                    }

                    track.artist = stringParts[0]
                    track.title = stringParts[0]
                    
                    if stringParts.count > 1 {
                        
                        let splitStringParts = stringParts[1].componentsSeparatedByString(",")
                        track.title = splitStringParts[0]
                        
                        track.artist = track.artist.stringByReplacingOccurrencesOfString("/", withString: " & ")
                    }
                }
                
                process()
                
                if track.artist.isEmpty && track.title.isEmpty {
                    
                    track.artist = station.desc!
                    track.title = station.name!
                }
                
                if currentSongName != track.title && (station.name != track.artist && station.name != track.title) {
                    
                    success(track: track, finished: (track.artist != station.name && track.title != station.name))
                }
            }
        }
    }
    
    private class func albumArtFromAmazon(track: Track, success: (track: Track, success: Bool) -> Void) {
        
        let amazonClient = AWSECommerceServiceClient.sharedClient()
        amazonClient.debug = false
        
        let itemSearchRequest = ItemSearchRequest()
        let title = "\(track.title)"
        let titleParts = title.componentsSeparatedByString("\'")
        itemSearchRequest.keywords = "\(track.artist) - \(titleParts[0])"
        RadioRecordLogger.Log(itemSearchRequest.keywords)
        
        let itemSearch = ItemSearch()
        
        itemSearch.associateTag = "tag"
        itemSearch.shared = ItemSearchRequest()
        itemSearch.shared.searchIndex = "MP3Downloads"
        itemSearch.shared.responseGroup = ["Images", "Large", "Medium", "Small"]
        itemSearch.request = [itemSearchRequest]
        
        amazonClient.authenticateRequest("ItemSearch")
        amazonClient.itemSearch(itemSearch, success: { (responseObject) -> Void in
        
            if responseObject.items != nil && responseObject.items.count > 0 {
                
                if let items = responseObject.items.objectAtIndex(0) as? Items {
                    
                    if items.item != nil && items.item.count > 0 {
                    
                        for item in items.item {
                            
                            if let item = item as? Item where item.itemAttributes.title.rangeOfString(track.title) != nil {
                                
                                var url: String? = nil
                                
                                if let largeImage = item.largeImage, largeImageUrl = largeImage.url {
                                    
                                    url = largeImageUrl
                                    
                                } else if let mediumImage = item.mediumImage, mediumImageUrl = mediumImage.url {
                                    
                                    url = mediumImageUrl
                                    
                                } else if let smallImage = item.smallImage, smallImageUrl = smallImage.url {
                                    
                                    url = smallImageUrl
                                }
                                
                                if let url = url, imageURL = NSURL(string: url) {
                                    
                                    RadioRecordLogger.Log("Amazon artURL: \(imageURL)")
                                    
                                    track.artworkURL = imageURL.absoluteString
                                    track.artworkLoaded = true
                                    
                                    success(track: track, success: true)
                                    return
                                    
                                } else {
                                    
                                    success(track: track, success: false)
                                    return
                                }
                            }
                        }
                        
                        success(track: track, success: false)
                        
                    } else {
                        
                        success(track: track, success: false)
                    }
                    
                } else {
                    
                    success(track: track, success: false)
                }
            
            } else {
                
                success(track: track, success: false)
            }
            
        }) { (error, soapFault) -> Void in
            
            success(track: track, success: false)
        }
    }
    
    private class func albumArtFromLastFm(track: Track, success: (track: Track, success: Bool) -> Void) {
        
        let queryURL = String(format: "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=%@&artist=%@&track=%@&format=json", LAST_FM_API_KEY, track.artist, track.title)
        
        if let escapedURL = queryURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
        
            MetadataManager.getTrackDataWithSuccess(escapedURL, success: { (metaData) -> Void in
                
                RadioRecordLogger.Log("url: \(escapedURL)")
                
                let json = JSON(data: metaData)
                
                if let imageArray = json["track"]["album"]["image"].array {
                    
                    let arrayCount = imageArray.count
                    let lastImage = imageArray[arrayCount - 1]
                    
                    if let artURL = lastImage["#text"].string {
                        
                        if artURL.rangeOfString("/noimage/") != nil {
                            
                            success(track: track, success: false)
                            
                        } else {
                            
                            RadioRecordLogger.Log("LastFm artURL: \(artURL)")
                            
                            track.artworkURL = artURL
                            track.artworkLoaded = true
                            
                            success(track: track, success: true)
                        }
                        
                    } else {
                        
                        success(track: track, success: false)
                    }
                    
                } else {
                    
                    success(track: track, success: false)
                }
            })
        }
    }

    private class func albumArtFromItunes(track: Track, success: (track: Track, success: Bool) -> Void) {
        
        let queryURL = String(format: "https://itunes.apple.com/search?term=%@+%@&entity=song", track.artist, track.title)
        
        if let escapedURL = queryURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
            
            MetadataManager.getTrackDataWithSuccess(escapedURL, success: { (metaData) -> Void in
                
                RadioRecordLogger.Log("url: \(escapedURL)")
                
                let json = JSON(data: metaData)
                
                if var artURL = json["results"][0]["artworkUrl100"].string {
                    
                    artURL = artURL.stringByReplacingOccurrencesOfString("100x100bb", withString: "1200x1200bb")
                    
                    RadioRecordLogger.Log("iTunes artURL: \(artURL)")
                    
                    track.artworkURL = artURL
                    track.artworkLoaded = true
                    
                    success(track: track, success: true)
                    
                } else {
                    
                    success(track: track, success: false)
                }
            })
        }
    }
    
    class func queryAlbumArt(track: Track, process: (type: ParserType) -> Void, success: (track: Track, type: ParserType, success: Bool) -> Void) {
        
        let saveCover = NSUserDefaults.standardUserDefaults().boolForKey(kSaveAlbumCoverKey)
        
        process(type: ParserType.Local)
        
        if let track = ImageManager.read(track) where saveCover {

            success(track: track, type: ParserType.Local, success: true)
            
        } else {
            
            process(type: ParserType.Amazon)
            
            MetadataManager.albumArtFromAmazon(track, success: { (track, finished) -> Void in
                
                if finished {
                    
                    success(track: track, type: ParserType.Amazon, success: true)
                
                } else {
                    
                    process(type: ParserType.LastFm)
                    
                    MetadataManager.albumArtFromLastFm(track, success: { (track, finished) -> Void in
                        
                        if finished {
                            
                            success(track: track, type: ParserType.LastFm, success: true)
                            
                        } else {
                            
                            process(type: ParserType.iTunes)
                            
                            MetadataManager.albumArtFromItunes(track, success: { (track, finished) -> Void in
                                
                                if finished {
                                    
                                    success(track: track, type: ParserType.iTunes, success: true)
                                    
                                } else {
                                    
                                    success(track: track, type: ParserType.iTunes, success: false)
                                }
                            })
                        }
                    })
                }
            })
        }
    }
    
    class func updateLockScreen(track: Track) {
        
        dispatch_async(dispatch_get_main_queue()) { 
            
            let albumArtwork = MPMediaItemArtwork(image: track.artworkImage!)
            
            MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = [
                
                MPMediaItemPropertyArtist: track.artist,
                MPMediaItemPropertyTitle: track.title,
                MPMediaItemPropertyArtwork: albumArtwork
            ]
        }
    }
}