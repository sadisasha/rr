//
//  NSLog.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 06.02.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

class RadioRecordLogger: NSObject {

    class func Log(items: Any..., separator: String = "", terminator: String = "") {
        
        if DEBUG_LOG { print(items) }
    }
}
