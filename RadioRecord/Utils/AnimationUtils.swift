//
//  AnimationUtils.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 12.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

class AnimationUtils: NSObject {

    static func createFrames() -> [UIImage] {

        var animationFrames = [UIImage]()
        
        for i in 0...3 {
            
            if let image = UIImage(named: "NowPlayingBars-\(i)")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate) {
                
                animationFrames.append(self.imageWithTintColor(image, tintColor: UIColor.grayColor()))
            }
        }
        
        for i in (1...2).reverse() {
            
            if let image = UIImage(named: "NowPlayingBars-\(i)")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate) {
                
                animationFrames.append(self.imageWithTintColor(image, tintColor: UIColor.grayColor()))
            }
        }
        
        return animationFrames
    }
    
    private static func imageWithTintColor(image: UIImage, tintColor: UIColor) -> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale);
        
        let context = UIGraphicsGetCurrentContext();
        CGContextTranslateCTM(context, 0, image.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        
        let rect = CGRectMake(0, 0, image.size.width, image.size.height);
        CGContextClipToMask(context, rect, image.CGImage)
        tintColor.setFill();
        CGContextFillRect(context, rect);
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage;
    }
}
