//
//  StationManager.swift
//  Record
//
//  Created by Aleksandr Sadikov on 09.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation

class StationManager: NSObject {
	
    class var sharedInstance: StationManager {
        
        struct Singleton {
            
            static let instance = StationManager()
        }
        
        return Singleton.instance
    }
    
    var stationList: Dictionary<String, AnyObject>? {
        
        get {
            
            if let path = NSBundle.mainBundle().pathForResource("Stations-Editing", ofType: "plist") {
            
                let dictionary = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject>
                
                return dictionary
            }
            
            return nil
        
        } set {
            
            if let path = NSBundle.mainBundle().pathForResource("Stations-Editing", ofType: "plist"), value = newValue {
                
                let dictionary = NSDictionary(dictionary: value)
                
                dictionary.writeToFile(path, atomically: true)
            }
        }
    }
    
    var stationCollection: StationCollection? {
        
        get {
            
            var collection: StationCollection? = nil
            
            if let list = self.stationList {
                
                collection = StationCollection()
                
                for key in list.keys {
                    
                    if let value = list[key] as? Dictionary<String, AnyObject> {
                        
                        var urls = Dictionary<String, NSURL>()
                        
                        for key in value.keys {
                            
                            if let item = value[key] as? String, url = NSURL(string: item) {
                                
                                urls.updateValue(url, forKey: key)
                            }
                        }
                        
                        let station = Station(name: key, urls: urls)
                        station.desc = key
                        collection?.append(station)
                    }
                }
            }
            
            return collection
        }
    }

    func addStation(station: Station?) {
        
        if var list = self.stationList, let key = station?.name, value = station?.urls {
            
            list.updateValue(value, forKey: key)
            
            self.stationList = list
        }
    }
    
    func removeStation(station: String) {
        
        if var list = self.stationList {

            list.removeValueForKey(station)
            
            self.stationList = list
        }
    }
    
    func resetStationList() -> Bool {
        
        if let sourcePath = NSBundle.mainBundle().pathForResource("Stations-Default", ofType: "plist"), targetPath = NSBundle.mainBundle().pathForResource("Stations-Editing", ofType: "plist") {
            
            let dictionary = NSDictionary(contentsOfFile: sourcePath)
            dictionary?.writeToFile(targetPath, atomically: true)
            
            return true
        }
        
        return false
    }
}
