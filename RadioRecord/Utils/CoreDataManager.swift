//
//  CoreDataManager.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 10.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager: NSObject {
    
    class var sharedInstance: CoreDataManager {
        
        struct Singleton {
            
            static let instance = CoreDataManager()
        }
        
        return Singleton.instance
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {

        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        
        return urls[urls.count - 1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {

        let modelURL = NSBundle.mainBundle().URLForResource("RadioRecord", withExtension: "momd")!
        
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {

        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        
        do {
            
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
            
        } catch {

            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)

            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {

        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext() {
        
        if managedObjectContext.hasChanges {
            
            do {
                
                try managedObjectContext.save()
                
            } catch {

                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func select() -> Array<AnyObject?>? {
        
        let fetchRequest = NSFetchRequest(entityName: "Station")

        do {
            
            let results = try self.managedObjectContext.executeFetchRequest(fetchRequest)
            let items = results as! Array<NSManagedObject>
            
            var fetchResult = Array<AnyObject?>()
            
            for item in items {
                
                fetchResult.append(item.valueForKey("name"))
            }
            
            return fetchResult
            
        } catch let error as NSError {
            
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func containts(item: AnyObject?) -> Bool {
        
        do {
            
            let fetchRequest = NSFetchRequest(entityName: "Station")
            
            let results = try self.managedObjectContext.executeFetchRequest(fetchRequest)
            let objects = results as! Array<NSManagedObject>
            
            for object in objects {
                
                if object.valueForKey("name") as? String == item as? String {
                    
                    return true
                }
            }
            
            return false
            
        } catch let error as NSError {
            
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
    }
    
    func insert(item: AnyObject?) -> Bool {

        let entity =  NSEntityDescription.entityForName("Station", inManagedObjectContext:self.managedObjectContext)
        let station = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: self.managedObjectContext)

        station.setValue(item, forKey: "name")

        do {
            
            try self.managedObjectContext.save()

            return true
            
        } catch let error as NSError {
            
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
    }

    func updateAtIndex(atIndex: Int, item: AnyObject?) -> Bool {
        
        do {
            
            let fetchRequest = NSFetchRequest(entityName: "Station")
            
            let results = try self.managedObjectContext.executeFetchRequest(fetchRequest)
            let objects = results as! Array<NSManagedObject>
            
            for (index, _) in objects.enumerate() {
                
                if atIndex == index {
                    
                    self.removeAtIndex(index)
                    self.insert(item)
                }
            }
            
            try self.managedObjectContext.save()
            
            return true
            
        } catch let error as NSError {
            
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
    }
    
    func removeAtIndex(atIndex: Int) -> Bool {
        
        do {
            
            let fetchRequest = NSFetchRequest(entityName: "Station")
            
            let results = try self.managedObjectContext.executeFetchRequest(fetchRequest)
            let items = results as! Array<NSManagedObject>
            
            for (index, item) in items.enumerate() {
                
                if atIndex == index { self.managedObjectContext.deleteObject(item) }
            }
            
            try self.managedObjectContext.save()
            
            return true
            
        } catch let error as NSError {
            
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
    }
    
    func removeAll() -> Bool {
        
        do {
            
            let fetchRequest = NSFetchRequest(entityName: "Station")
            
            let results = try self.managedObjectContext.executeFetchRequest(fetchRequest)
            let items = results as! Array<NSManagedObject>
            
            for item in items {
                
                self.managedObjectContext.deleteObject(item)
            }
            
            try self.managedObjectContext.save()
            
            return true
            
        } catch let error as NSError {
            
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
    }
}
