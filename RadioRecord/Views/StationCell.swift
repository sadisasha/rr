//
//  SideMenuCell.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 10.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

class StationCell: UITableViewCell {

    @IBOutlet var stationImage: UIImageView!
    @IBOutlet var stationTitle: UILabel!
    @IBOutlet weak var checkedImage: UIImageView!

    override func awakeFromNib() {

        self.stationImage.layer.cornerRadius = self.stationImage.frame.width / 2
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
