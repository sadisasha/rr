//
//  ToastManager.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 13.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

enum ToastPosition: String {
    
    case Top = "Top", Center = "Center", Bottom = "Bottom"
}

class ManagerHelper {
    
    var sharedStyle: ToastStyle
    var tapToDismissEnabled: Bool
    var queueEnabled: Bool
    var defaultDuration: NSTimeInterval
    var defaultPosition: ToastPosition
    
    class var sharedInstance: ManagerHelper {
        
        struct Singleton {
            
            static let instance = ManagerHelper()
        }
        
        return Singleton.instance
    }
    
    init() {
        
        self.sharedStyle = ToastStyle()
        self.tapToDismissEnabled = true
        self.queueEnabled = true
        self.defaultDuration = 3.0
        self.defaultPosition = .Bottom
    }
}

class ToastManager {
    
    static var sharedStyle: ToastStyle {
        
        get {
            
            return ManagerHelper.sharedInstance.sharedStyle
            
        } set {
            
            ManagerHelper.sharedInstance.sharedStyle = newValue
        }
    }
    
    static var tapToDismissEnabled: Bool {
        
        get {
            
            return ManagerHelper.sharedInstance.tapToDismissEnabled
            
        } set {
            
            ManagerHelper.sharedInstance.tapToDismissEnabled = newValue
        }
    }
    
    static var queueEnabled: Bool {
        
        get {
            
            return ManagerHelper.sharedInstance.queueEnabled
            
        } set {
            
            ManagerHelper.sharedInstance.queueEnabled = newValue
        }
    }
    
    static var defaultDuration: NSTimeInterval {
        
        get {
            
            return ManagerHelper.sharedInstance.defaultDuration
            
        } set {
            
            ManagerHelper.sharedInstance.defaultDuration = newValue
        }
    }
    
    static var defaultPosition: ToastPosition {
        
        get {
            
            return ManagerHelper.sharedInstance.defaultPosition
            
        } set {
            
            ManagerHelper.sharedInstance.defaultPosition = newValue
        }
    }
}
