//
//  ToastStyle.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 13.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

class ToastStyle {

    var backgroundColor: UIColor
    var titleColor: UIColor
    var messageColor: UIColor
    var shadowColor: UIColor
    
    var maxWidthPercentage: CGFloat
    var maxHeightPercentage: CGFloat
    var horizontalPadding: CGFloat
    var verticalPadding: CGFloat
    var cornerRadius: CGFloat
    var shadowOpacity: CGFloat
    var shadowRadius: CGFloat
    
    var titleFont: UIFont
    var messageFont: UIFont
    
    var titleAlignment: NSTextAlignment
    var messageAlignment: NSTextAlignment
    
    var titleNumberOfLines: Int
    var messageNumberOfLines: Int
    
    var displayShadow: Bool
    
    var shadowOffset: CGSize
    var imageSize: CGSize
    var activitySize: CGSize
    
    var fadeDuration: NSTimeInterval
    
    init() {
        
        self.backgroundColor = UIColor(red: 89/255, green: 89/255, blue: 89/255, alpha: 0.85)
        self.titleColor = UIColor.whiteColor()
        self.messageColor = UIColor.whiteColor()
        self.shadowColor = UIColor.grayColor()
        self.maxWidthPercentage = 0.8
        self.maxHeightPercentage = 0.8
        self.horizontalPadding = 6.0
        self.verticalPadding = 4.0
        self.cornerRadius = 8.0
        self.titleFont = RadioRecordToastTitleFont!
        self.messageFont = RadioRecordToastMessageFont!
        self.titleAlignment = NSTextAlignment.Left
        self.messageAlignment = NSTextAlignment.Left
        self.titleNumberOfLines = 0
        self.messageNumberOfLines = 0
        self.displayShadow = false
        self.shadowOpacity = 0.8
        self.shadowRadius = 6.0
        self.shadowOffset = CGSizeMake(4.0, 4.0)
        self.imageSize = CGSizeMake(80.0, 80.0)
        self.activitySize = CGSizeMake(100.0, 100.0)
        self.fadeDuration = 0.2
    }
}
