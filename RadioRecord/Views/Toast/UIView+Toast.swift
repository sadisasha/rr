//
//  UIView+Toast.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 13.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

class ClosureWrapper {
    
    var closure: ((tapped: Bool) -> Void)?
    
    init(closure: ((tapped: Bool) -> Void)?) {
        
        self.closure = closure
    }
}

// Keys for values associated with toast views
let ToastTimerKey             = "ToastTimerKey"
let ToastDurationKey          = "ToastDurationKey"
let ToastPositionKey          = "ToastPositionKey"
let ToastCompletionKey        = "ToastCompletionKey"

// Keys for values associated with self
let ToastActiveToastViewKey   = "ToastActiveToastViewKey"
let ToastActivityViewKey      = "ToastActivityViewKey"
let ToastQueueKey             = "ToastQueueKey"

extension UIView {
    
    //MARK: - Make Toast Methods
    
    func makeToast(message: String) {
        
        self.makeToast(message, duration: ToastManager.defaultDuration, position: ToastManager.defaultPosition, style: nil)
    }
    
    func makeToast(message: String, duration: NSTimeInterval, position: ToastPosition) {
        
        self.makeToast(message, duration: duration, position: position, style: nil)
    }
    
    func makeToast(message: String, duration: NSTimeInterval, position: ToastPosition, style: ToastStyle?) {

        let toast = self.toastViewForMessage(message, title: nil, image: nil, style: style)
        self.showToast(toast, duration: duration, position: position, completion: nil)
    }
    
    func makeToast(message: String, duration: NSTimeInterval, position: ToastPosition, title: String?, image: UIImage?, style: ToastStyle?, completion: ((tapped: Bool) -> Void)?) {
        
        let toast = self.toastViewForMessage(message, title: title, image: image, style: style)
        self.showToast(toast, duration: duration, position: position, completion: completion)
    }

    //MARK: - Show Toast Methods

    func showToast(toast: UIView?) {
        
        self.showToast(toast, duration: ToastManager.defaultDuration, position: ToastManager.defaultPosition, completion: nil)
    }

    func showToast(toast: UIView?, duration: NSTimeInterval, position: ToastPosition, completion: ((tapped: Bool) -> Void)?) {
        
        if let toast = toast {
            
            objc_setAssociatedObject(toast, ToastCompletionKey, ClosureWrapper(closure: completion), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            if ToastManager.queueEnabled && objc_getAssociatedObject(self, ToastActiveToastViewKey) != nil {
                
                objc_setAssociatedObject(toast, ToastDurationKey, duration, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                objc_setAssociatedObject(toast, ToastPositionKey, position.hashValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                
                var toastQueue = self.cs_toastQueue()
                toastQueue.append(toast)
            
            } else {

                self.cs_showToast(toast, duration: duration, position: position)
            }
        }
    }

    //MARK: - Private Show/Hide Methods
    
    func cs_showToast(toast: UIView?, duration: NSTimeInterval, position: ToastPosition) {
        
        if let toast = toast {
            
            toast.center = self.cs_centerPointForPosition(position, toast: toast)
            toast.alpha = 0.0
            
            if ToastManager.tapToDismissEnabled {
                
                let recognizer = UITapGestureRecognizer(target: self, action: #selector(cs_handleToastTapped(_:)))
                toast.addGestureRecognizer(recognizer)
                toast.userInteractionEnabled = true
                toast.exclusiveTouch = true
            }
            
            objc_setAssociatedObject(self, ToastActiveToastViewKey, toast, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            self.addSubview(toast)
            
            UIView.animateWithDuration(ToastManager.sharedStyle.fadeDuration, delay: 0.0, options: [.CurveEaseOut, .AllowUserInteraction], animations: { () -> Void in
                
                toast.alpha = 1.0
                
                }, completion: { (finished) -> Void in
                    
                    let timer = NSTimer(timeInterval: duration, target: self, selector: #selector(self.cs_toastTimerDidFinish(_:)), userInfo: toast, repeats: false)
                    NSRunLoop.mainRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
                    
                    objc_setAssociatedObject(toast, ToastTimerKey, timer, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            })
        }
        
        
    }

    func cs_hideToast(toast: UIView) {
        
        self.cs_hideToast(toast, fromTap: false)
    }
    
    func cs_hideToast(toast: UIView, fromTap: Bool) {
        
        UIView.animateWithDuration(ToastManager.sharedStyle.fadeDuration, delay: 0.0, options: [.CurveEaseIn, .BeginFromCurrentState], animations: { () -> Void in
            
            toast.alpha = 0.0
            
            }) { (finished) -> Void in
            
                toast.removeFromSuperview()
                
                objc_setAssociatedObject(self, ToastActiveToastViewKey, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                
                if let wrapper = objc_getAssociatedObject(toast, ToastCompletionKey) as? ClosureWrapper {
                   
                    wrapper.closure?(tapped: fromTap)
                }
                
                if self.cs_toastQueue().count > 0 {
                    
                    var toastQueue = self.cs_toastQueue()
                    
                    if let nextToast = toastQueue.first as? UIView {
                     
                        toastQueue.removeAtIndex(0)
                        
                        let duration: NSTimeInterval = objc_getAssociatedObject(nextToast, ToastDurationKey).doubleValue
                        let position: ToastPosition = objc_getAssociatedObject(nextToast, ToastPositionKey) as! ToastPosition
                        
                        self.cs_showToast(nextToast, duration: duration, position: position)
                    }
                }
                
                
        }
    }

    //MARK: - View Construction
    
    func toastViewForMessage(message: String?, title: String?, image: UIImage?, style: ToastStyle?) -> UIView? {
        
        var toastStyle = style
        
        if message == nil && title == nil && image == nil { return nil }

        if toastStyle == nil { toastStyle = ToastManager.sharedStyle }

        var messageLabel: UILabel? = nil
        var titleLabel: UILabel? = nil
        var imageView: UIImageView? = nil
        
        let wrapperView = UIView()
        wrapperView.autoresizingMask = [.FlexibleLeftMargin , .FlexibleRightMargin, .FlexibleTopMargin, .FlexibleBottomMargin]
        wrapperView.layer.cornerRadius = toastStyle!.cornerRadius
        
        if toastStyle!.displayShadow {
            
            wrapperView.layer.shadowColor = toastStyle!.shadowColor.CGColor
            wrapperView.layer.shadowOpacity = Float(toastStyle!.shadowOpacity)
            wrapperView.layer.shadowRadius = toastStyle!.shadowRadius
            wrapperView.layer.shadowOffset = toastStyle!.shadowOffset
        }
        
        wrapperView.backgroundColor = toastStyle!.backgroundColor
        
        if image != nil {
            
            imageView = UIImageView(image: image)
            imageView?.contentMode = UIViewContentMode.ScaleAspectFit;
            imageView?.frame = CGRectMake(toastStyle!.horizontalPadding, toastStyle!.verticalPadding, toastStyle!.imageSize.width, toastStyle!.imageSize.height);
        }
        
        var imageRect = CGRectZero
        
        if imageView != nil {
            
            imageRect.origin.x = toastStyle!.horizontalPadding
            imageRect.origin.y = toastStyle!.verticalPadding
            imageRect.size.width = imageView!.bounds.size.width
            imageRect.size.height = imageView!.bounds.size.height
        }
        
        if title != nil {
            
            titleLabel = UILabel()
            titleLabel?.numberOfLines = toastStyle!.titleNumberOfLines
            titleLabel?.font = toastStyle!.titleFont
            titleLabel?.textAlignment = toastStyle!.titleAlignment
            titleLabel?.lineBreakMode = NSLineBreakMode.ByTruncatingTail
            titleLabel?.textColor = toastStyle!.titleColor
            titleLabel?.backgroundColor = UIColor.clearColor()
            titleLabel?.alpha = 1.0
            titleLabel?.text = title!

            let maxSizeTitle = CGSizeMake((self.bounds.size.width * toastStyle!.maxWidthPercentage) - imageRect.size.width, self.bounds.size.height * toastStyle!.maxHeightPercentage);
            var expectedSizeTitle = titleLabel!.sizeThatFits(maxSizeTitle)

            expectedSizeTitle = CGSizeMake(min(maxSizeTitle.width, expectedSizeTitle.width), min(maxSizeTitle.height, expectedSizeTitle.height))
            titleLabel?.frame = CGRectMake(0.0, 0.0, expectedSizeTitle.width, expectedSizeTitle.height)
        }
        
        if message != nil {
            
            messageLabel = UILabel()
            messageLabel?.numberOfLines = toastStyle!.messageNumberOfLines
            messageLabel?.font = toastStyle!.messageFont
            messageLabel?.textAlignment = toastStyle!.messageAlignment
            messageLabel?.lineBreakMode = NSLineBreakMode.ByTruncatingTail
            messageLabel?.textColor = toastStyle!.messageColor
            messageLabel?.backgroundColor = UIColor.clearColor()
            messageLabel?.alpha = 1.0
            messageLabel?.text = message!
            
            let maxSizeMessage = CGSizeMake((self.bounds.size.width * toastStyle!.maxWidthPercentage) - imageRect.size.width, self.bounds.size.height * toastStyle!.maxHeightPercentage)
            var expectedSizeMessage = messageLabel!.sizeThatFits(maxSizeMessage)

            expectedSizeMessage = CGSizeMake(min(maxSizeMessage.width, expectedSizeMessage.width), min(maxSizeMessage.height, expectedSizeMessage.height))
            messageLabel?.frame = CGRectMake(0.0, 0.0, expectedSizeMessage.width, expectedSizeMessage.height)
        }
        
        var titleRect = CGRectZero;
        
        if titleLabel != nil {
            
            titleRect.origin.x = imageRect.origin.x + imageRect.size.width + toastStyle!.horizontalPadding
            titleRect.origin.y = toastStyle!.verticalPadding
            titleRect.size.width = titleLabel!.bounds.size.width
            titleRect.size.height = titleLabel!.bounds.size.height
        }
        
        var messageRect = CGRectZero;
        
        if messageLabel != nil {
        
            messageRect.origin.x = imageRect.origin.x + imageRect.size.width + toastStyle!.horizontalPadding
            messageRect.origin.y = titleRect.origin.y + titleRect.size.height + toastStyle!.verticalPadding
            messageRect.size.width = messageLabel!.bounds.size.width
            messageRect.size.height = messageLabel!.bounds.size.height
        }
        
        let longerWidth = max(titleRect.size.width, messageRect.size.width)
        let longerX = max(titleRect.origin.x, messageRect.origin.x)

        let wrapperWidth = max((imageRect.size.width + (toastStyle!.horizontalPadding * 2.0)), (longerX + longerWidth + toastStyle!.horizontalPadding))
        let wrapperHeight = max((messageRect.origin.y + messageRect.size.height + toastStyle!.verticalPadding), (imageRect.size.height + (toastStyle!.verticalPadding * 2.0)))
        
        wrapperView.frame = CGRectMake(0.0, 0.0, wrapperWidth, wrapperHeight)
        
        if titleLabel != nil {
            
            titleLabel?.frame = titleRect
            wrapperView.addSubview(titleLabel!)
        }
        
        if messageLabel != nil {
            
            messageLabel?.frame = messageRect
            wrapperView.addSubview(messageLabel!)
        }
        
        if imageView != nil {
            
            wrapperView.addSubview(imageView!)
        }
        
        return wrapperView
    }

    //MARK: - Queue
    
    func cs_toastQueue() -> Array<AnyObject> {
    
        var cs_toastQueue = objc_getAssociatedObject(self, ToastQueueKey) as? Array<AnyObject>
        
        if cs_toastQueue == nil {
            
            cs_toastQueue = Array()
            objc_setAssociatedObject(self, ToastQueueKey, cs_toastQueue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }

        return cs_toastQueue!
    }
    
    //MARK: - Events
    
    func cs_toastTimerDidFinish(timer: NSTimer?) {
        
        if let toast = timer?.userInfo as? UIView {
            
            self.cs_hideToast(toast)
        }
    }
    
    func cs_handleToastTapped(recognizer: UITapGestureRecognizer?) {
       
        if let toast = recognizer?.view {
            
            let timer = objc_getAssociatedObject(toast, ToastTimerKey) as! NSTimer
            timer.invalidate()
            
            self.cs_hideToast(toast, fromTap: true)
        }
    }
    
    //MARK: - Activity Methods
    
    func makeToastActivity(position: ToastPosition) {
        
        let existingActivityView = objc_getAssociatedObject(self, ToastActivityViewKey) as? UIView
        if existingActivityView != nil { return }
        
        let style = ToastManager.sharedStyle
        
        let activityView = UIView(frame: CGRectMake(0.0, 0.0, style.activitySize.width, style.activitySize.height))
        activityView.center = self.cs_centerPointForPosition(position, toast: activityView)
        activityView.backgroundColor = style.backgroundColor
        activityView.alpha = 0.0
        activityView.autoresizingMask = [.FlexibleLeftMargin, .FlexibleRightMargin, .FlexibleTopMargin, .FlexibleBottomMargin]
        activityView.layer.cornerRadius = style.cornerRadius
        
        if style.displayShadow {
            
            activityView.layer.shadowColor = style.shadowColor.CGColor
            activityView.layer.shadowOpacity = Float(style.shadowOpacity)
            activityView.layer.shadowRadius = style.shadowRadius
            activityView.layer.shadowOffset = style.shadowOffset
        }
        
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        activityIndicatorView.center = CGPointMake(activityView.bounds.size.width / 2, activityView.bounds.size.height / 2)
        activityView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        objc_setAssociatedObject (self, ToastActivityViewKey, activityView, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        
        self.addSubview(activityView)
        
        UIView.animateWithDuration(style.fadeDuration, delay: 0.0, options: .CurveEaseOut, animations: { () -> Void in
            
            activityView.alpha = 1.0
            
            }, completion: nil)
    }
    
    func hideToastActivity() {
        
        if let existingActivityView = objc_getAssociatedObject(self, ToastActivityViewKey) as? UIView {
            
            UIView.animateWithDuration(ToastManager.sharedStyle.fadeDuration, delay: 0.0, options: [.CurveEaseIn, .BeginFromCurrentState], animations: { () -> Void in
                
                existingActivityView.alpha = 0.0
                
                }, completion: { (finished) -> Void in
                    
                    existingActivityView.removeFromSuperview()
                    
                    objc_setAssociatedObject(self, ToastActivityViewKey, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            })
        }
    }
    
    //MARK: - Helpers
    
    func cs_centerPointForPosition(position: ToastPosition, toast: UIView) -> CGPoint {
        
        let style = ToastManager.sharedStyle
        
        switch position {
            
        case .Top:
            return CGPointMake(self.bounds.size.width / 2, (toast.frame.size.height / 2) + style.verticalPadding)
        case .Center:
            return CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2)
        case .Bottom:
            return CGPointMake(self.bounds.size.width/2, (self.bounds.size.height - (toast.frame.size.height / 2)) - style.verticalPadding)
        }
    }
}