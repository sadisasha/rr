//
//  StationListController.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 10.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit
import AVFoundation

class StationListController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var backgroundImageView : UIImageView!
    
    @IBOutlet var dimmerView  : UIView!
    
    @IBOutlet var tableView: UITableView!
    
    var items: Array<Station> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.rightBarButtonItem?.enabled = false
        self.navigationItem.rightBarButtonItem?.image = nil
        
        self.navigationItem.title(NSLocalizedString("stationListTitle", comment: "Station list title"))

        self.dimmerView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
        
        self.backgroundImageView.image = UIImage(named: "stationlist-bg")
        
        self.tableView.separatorStyle = .None
        self.tableView.backgroundColor = UIColor.clearColor()
        
        if let stations = StationManager.sharedInstance.stationCollection?.array {
            
            self.items.removeAll()
            
            var checkedIndex = 0
            
            for (index, station) in stations.enumerate() {
                
                station.checked = CoreDataManager.sharedInstance.containts(station.name)
                if let checked = station.checked where checked { checkedIndex = index }
                self.items.append(station)
            }
            
            self.tableView.reloadData()

            if self.items.count > 0 {
                
                let indexPath = NSIndexPath(forRow: checkedIndex, inSection: 0)
                self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: false)
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder)]
        Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StationCell") as! StationCell
        
        let item = self.items[indexPath.row]
        cell.stationImage.image = UIImage(named: "\(item.name!)-square")
        
        cell.stationTitle.font = RadioRecordTableViewCellTitleFont
        cell.stationTitle.textColor = UIColor.whiteColor()
        
        cell.stationTitle.text = item.name!
        
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = .None
        
        if let checked = item.checked {

            cell.checkedImage.image = checked ? UIImage(named: "checkbox") : UIImage(named: "checkbox_outline")
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = self.items[indexPath.row]
        
        if let checked = item.checked {
            
            item.checked = !checked
        }
        
        self.items.replace(item, atIndex: indexPath.row)
        
        self.writeStationList()
        
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        return true
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let favorite = UITableViewRowActionWithImage.rowActionWithStyle(UITableViewRowActionStyle.Default, title: NSLocalizedString("favoriteButtonTitle", comment: "Favorite button title"), backgroundColor: UIColor.orangeColor(), image: UIImage(named: "ic_star")!, forCellHeight: UInt(tableView.rowHeight)) { (row, index) -> Void in
            
            tableView.setEditing(false, animated: true)
            
            self.items[index.row].checked = true
            self.writeStationList()
            tableView.reloadData()
        }
        
        let delete = UITableViewRowActionWithImage.rowActionWithStyle(UITableViewRowActionStyle.Default, title: NSLocalizedString("deleteButtonTitle", comment: "Delete button title"), backgroundColor: UIColor.redColor(), image: UIImage(named: "ic_delete")!, forCellHeight: UInt(tableView.rowHeight)) { (row, index) -> Void in
            
            tableView.setEditing(false, animated: true)
            
            if FreeStreamer.sharedInstance.currentStationName != self.items[indexPath.row].name {
                
                if let name = self.items[indexPath.row].name {
                    
                    StationManager.sharedInstance.removeStation(name)
                }
                
                if let checked = self.items[indexPath.row].checked where checked {
                    
                    self.items[indexPath.row].checked = false
                }
                
                self.items.removeAtIndex(indexPath.row)
                
                self.writeStationList()
                
                tableView.reloadData()
                
            } else {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    self.tableView.setEditing(false, animated: true)
                    
                    SCLAlertView().showTitle("RadioRecord", subTitle: NSLocalizedString("cantDeleteStationAsPlayer", comment: "Cant delete station as player title"), duration: 5.0, completeText: NSLocalizedString("cancelButtonTitle", comment: "Cancel button title"), style: SCLAlertViewStyle.Error, colorStyle: 0xC1272D, colorTextButton: 0xFFFFFF)
                })
            }
        }
        
        return [delete, favorite]
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {

    }
    
    func dismiss(sender: AnyObject?) {

        self.tableView.setEditing(false, animated: true)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func writeStationList() {
        
        CoreDataManager.sharedInstance.removeAll()
        
        for item in self.items {
            
            if let checked = item.checked where checked {
                
                CoreDataManager.sharedInstance.insert(item.name)
            }
        }
    }
    
    @IBAction func onLeftButtonAction(sender: UIBarButtonItem?) {

        self.dismiss(nil)
    }

    @IBAction func onRightButtonAction(sender: UIBarButtonItem?) {

        self.dismiss(nil)
    }
}
