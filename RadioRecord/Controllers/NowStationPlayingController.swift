//
//  StationPlayerController.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 10.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit
import MediaPlayer
import Spring
import SDWebImage

protocol NowStationPlayingControllerDelegate: class {
    
    func didSongMetaDataUpdate(track: Track)
    func didArtworkUpdate(track: Track)
    func trackPlayingToggled(track: Track)
}

class NowStationPlayingController: UIViewController, FreeStreamerDelegate {

    weak var delegate: NowStationPlayingControllerDelegate?
    
    var track: Track!
    var currentStation: Station?
    var newStation = true

    @IBOutlet var backgroundImageView : UIImageView!
    @IBOutlet var dimmerView  : UIView!

    @IBOutlet weak var albumImageView: SpringImageView!
    
    @IBOutlet weak var bottomMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var albumWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var albumHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var songLabel: SpringLabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var volumeParentView: UIView!
    @IBOutlet weak var volumeSlider: UISlider!
    
    private var nowPlayingImageView: UIImageView!
    private let radioPlayer = FreeStreamer.sharedInstance
    private var iPhone4 = false
    private var justBecameActive = false
    
    private var navigationItemTitle: UILabel!
    
    //MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        FreeStreamer.sharedInstance.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didBecomeActiveNotificationReceived(_:)), name:"UIApplicationDidBecomeActiveNotification", object: nil)
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder)]
        Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
        
        if self.newStation {
            
            self.track = Track()
            self.stationDidChange()
            
        } else {
            
            if let track = self.track {
                
                self.updateAlbumArtwork()
                
                if track.isPlaying {

                    self.onPlayAction()
                    
                    self.updateLabels()
                    nowPlayingImageView.startAnimating()
                
                } else {
                    
                    self.updateLabels(NSLocalizedString("stationPausedTitle", comment: "Station paused title"))
                    self.playButtonEnable()
                }
                
            } else {
                
                self.track = Track()
                self.stationDidChange()
            }
        }
        
        self.setupVolumeSlider()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        FreeStreamer.sharedInstance.delegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {

        NSNotificationCenter.defaultCenter().removeObserver(self, name:"UIApplicationDidBecomeActiveNotification", object: nil)
    }
    
    //MARK: - NSNotificationCenter handle
        
    func didBecomeActiveNotificationReceived(notification: NSNotification?) {
        
        self.updateLabels()
        self.justBecameActive = true
        self.updateAlbumArtwork()
    }
    
    //MARK: - Setup

    func optimizeForDeviceSize() {

        let deviceHeight = self.view.bounds.height
        
        let frameSize = self.view.frame.size
        
        if deviceHeight == 480 {
            
            self.iPhone4 = true
            
            self.albumWidthConstraint.constant = frameSize.width - 40
            self.albumHeightConstraint.constant = frameSize.width - 100
            
            self.bottomMarginConstraint.constant = 30
            
        } else if deviceHeight == 667 {
            
            self.albumWidthConstraint.constant = frameSize.width - 40
            self.albumHeightConstraint.constant = frameSize.width - 40
            
            self.bottomMarginConstraint.constant = 50
            
        } else if deviceHeight > 667 {

            self.albumWidthConstraint.constant = frameSize.width - 40
            self.albumHeightConstraint.constant = frameSize.width - 40
            
            self.bottomMarginConstraint.constant = 70
        }
        
        self.view.updateConstraints()
    }
    
    func setupUI() {
        
        //self.createVisualizerView()
        self.createNowPlayingAnimation()
        self.updateToolbarTitle()
        
        self.backgroundImageView.image = UIImage(named: "player-bg")
        self.dimmerView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        
        self.optimizeForDeviceSize()
        self.setupPlayer()
    }
    
    func setupPlayer() {

        self.radioPlayer.prepareToPlay()
    }
    
    func setupVolumeSlider() {
        
        self.volumeParentView.backgroundColor = UIColor.clearColor()

        self.volumeSlider?.setThumbImage(UIImage(named: "slider-ball"), forState: .Normal)
        
        self.volumeSlider.value = self.radioPlayer.outputVolume!
    }
    
    func updateToolbarTitle() {
        
        if let stationName = self.currentStation?.name {
            
            let font = RadioRecordNavigationBarTitleFont
            
            let size = (stationName as NSString).sizeWithAttributes([NSFontAttributeName: font!])
            let frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            
            self.navigationItemTitle = UILabel(frame: frame)
            
            self.navigationItemTitle.textAlignment = NSTextAlignment.Center
            self.navigationItemTitle.text = stationName
            self.navigationItemTitle.textColor = UIColor.lightTextColor()
            self.navigationItemTitle.font = font
            
            self.navigationItem.titleView = self.navigationItemTitle
        }
    }
    
    func updateLabels(statusMessage: String = "") {
        
        if statusMessage != "" {

            self.songLabel.text = statusMessage
            self.artistLabel.text = self.currentStation?.name
            
        } else {

            if self.track != nil {
                
                self.songLabel.text = track.title
                self.artistLabel.text = track.artist
            }
        }
    }
    
    func stationDidChange() {

        var kbpsString: String?
        
        let bitrate = NSUserDefaults.standardUserDefaults().integerForKey(kSoundQualityBitrateKey)
        if bitrate == 0 {
            
            kbpsString = "32"
            
        } else if bitrate == 1 {
            
            kbpsString = "128"
            
        } else {
            
            kbpsString = "320"
        }
        
        self.track.bitrate = bitrate
        
        if let kbpsKey = kbpsString, station = self.currentStation, url = station.urls?[kbpsKey] {

            self.radioPlayer.stop()
            
            let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder), "Action": "open url \(url)"]
            Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
            
            RadioRecordLogger.Log("Play station with url: \(url)")

            self.radioPlayer.currentStationName = station.name
            self.radioPlayer.currentURL = url
            self.radioPlayer.prepareToPlay()
            self.radioPlayer.play()
            
            self.updateLabels(NSLocalizedString("loadingStationTitle", comment: "Loading station title"))

            self.songLabel.animation = "flash"
            self.songLabel.repeatCount = 2
            self.songLabel.animate()
            
            self.clearAlbumArtwork()
            
            self.track.isPlaying = true
        }
    }
    
    func clearAlbumArtwork() {
        
        self.track.artworkLoaded = false
        self.track.artworkURL = self.currentStation!.name!
        
        if !self.track.artworkURL.isEmpty {
            
            self.albumImageView.image = UIImage()
            self.track.artworkImage = albumImageView.image
            self.track.artworkLoaded = true
            
            self.delegate?.didArtworkUpdate(self.track)
            
        } else {
            
            self.albumImageView.image = UIImage()
            self.track.artworkImage = albumImageView.image
        }
        
        self.view.setNeedsDisplay()
    }
    
    func updateAlbumArtwork(type: ParserType = .Local) {
        
        if type == .Local {

            self.track.artworkLoaded = true
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.albumImageView.image = self.track.artworkImage
                
                self.albumImageView.animation = "wobble"
                self.albumImageView.duration = 2
                self.albumImageView.animate()
            })
            
            MetadataManager.updateLockScreen(self.track)
            
            self.delegate?.didArtworkUpdate(self.track)
        
        } else {
            
            self.track.artworkLoaded = false
            if self.track.artworkURL.rangeOfString("http") != nil {
                
                if let url = NSURL(string: self.track.artworkURL) {
                    
                    SDWebImageDownloader.sharedDownloader().downloadImageWithURL(url, options: SDWebImageDownloaderOptions.HighPriority, progress: { (receivedSize, expectedSize) -> Void in
                        
                        }, completed: { (image, data, error, finished) -> Void in
                            
                            if let image = image where finished {
                                
                                self.track.artworkImage = image
                                self.track.artworkLoaded = true
                                
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    
                                    self.albumImageView.image = self.track.artworkImage
                                    
                                    self.albumImageView.animation = "wobble"
                                    self.albumImageView.duration = 2
                                    self.albumImageView.animate()
                                })
                                
                                if NSUserDefaults.standardUserDefaults().boolForKey(kSaveAlbumCoverKey) && type != .Local {
                                    
                                    ImageManager.save(self.track, type: type == .LastFm ? ExtType.PNG : ExtType.JPG)
                                }
                                
                                MetadataManager.updateLockScreen(self.track)
                                
                                self.delegate?.didArtworkUpdate(self.track)
                            }
                    })
                }
            }
        }

        if self.track.artworkLoaded && !self.justBecameActive {
            
            self.justBecameActive = false
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
            self.view.setNeedsDisplay()
        }
    }
    
    func playButtonEnable(enabled: Bool = true) {
        
        self.playButton.enabled = enabled
        self.pauseButton.enabled = !enabled
        self.track.isPlaying = !enabled
    }
    
//    func createVisualizerView() {
//        
//        let visualizerView = FrequencyVisualizerView(frame: self.visualizerView.frame)
//        self.visualizerView.addSubview(visualizerView)
//        
//        self.visualizerView.hidden = true
//        
//        let analyzer = FSFrequencyDomainAnalyzer()
//        analyzer.delegate = visualizerView
//        analyzer.enabled = true
//        
//        FreeStreamer.sharedInstance.stream.delegate = analyzer
//    }
    
    func createNowPlayingAnimation() {
        
        let image = UIImage(named: "NowPlayingBars-3")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.nowPlayingImageView = UIImageView(image: image)
        self.nowPlayingImageView.autoresizingMask = UIViewAutoresizing.None
        self.nowPlayingImageView.contentMode = UIViewContentMode.Center

        self.nowPlayingImageView.animationImages = AnimationUtils.createFrames()
        self.nowPlayingImageView.animationDuration = 0.8

        let barButton = UIButton(type: UIButtonType.Custom)
        barButton.frame = CGRectMake(0, -6, 22, 22);
        barButton.addSubview(self.nowPlayingImageView)
        self.nowPlayingImageView.center = barButton.center

        self.navigationItem.rightBarButtonItem?.customView = barButton
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.grayColor()
        self.nowPlayingImageView.tintColor = UIColor.grayColor()
    }
    
    func startNowPlayingAnimation() {
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
            self.nowPlayingImageView.startAnimating()
            self.nowPlayingImageView.tintColor = UIColor.grayColor()
        }
    }
    
    func dismiss(sender: AnyObject?) {

        self.navigationController?.popViewControllerAnimated(true)
    }
        
    //MARK: - IBAction
    
    @IBAction func onPlayAction() {
        
        self.track.isPlaying = true
        self.playButtonEnable(false)
        self.radioPlayer.play()
        self.updateLabels()

        self.songLabel.animation = "flash"
        self.songLabel.animate()

        self.nowPlayingImageView.startAnimating()

        self.delegate?.trackPlayingToggled(self.track)
    }
    
    @IBAction func onPauseAction() {
        
        self.track.isPlaying = false
        
        self.playButtonEnable()
        
        self.radioPlayer.pause()
        self.updateLabels(NSLocalizedString("stationPausedTitle", comment: "Station paused title"))
        self.nowPlayingImageView.stopAnimating()

        self.delegate?.trackPlayingToggled(self.track)
    }
    
    @IBAction func onVolumeChanged(sender: UISlider?) {

        if let sender = sender {

            self.radioPlayer.setVolume(sender.value)
        }
    }

    @IBAction func onLeftButtonAction(sender: UIBarButtonItem?) {
        
        self.dismiss(nil)
    }
    
    //MARK: - FreeStreamerDelegate methods
    
    func freeStreamer(freeStreamer: FSAudioController?, onStateChange: FSAudioStreamState?) {
      
        if let state = onStateChange {
            
            var changeCategory = "..."
            
            switch state {
                
            case kFsAudioStreamRetrievingURL:
                changeCategory = "Retrieving URL..."
                break
                
            case kFsAudioStreamStopped:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                changeCategory = "Stopped..."
                break

            case kFsAudioStreamPaused:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                changeCategory = "Stopped..."
                break
                
            case kFsAudioStreamBuffering:
                changeCategory = "Buffering..."
                break
                
            case kFsAudioStreamSeeking:
                changeCategory = "Seeking..."
                break
                
            case kFsAudioStreamPlaying:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                changeCategory = "Playing..."
                break
                
            case kFsAudioStreamFailed:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                changeCategory = "Failed..."
                break
                
            default: break
            }
            
            RadioRecordLogger.Log(changeCategory)
        }
    }
    
    func freeStreamer(freeStreamer: FSAudioController?, onFailure: FSAudioStreamError?, description: String?) {
       
        if let state = onFailure {
            
            var errorCategory = "..."
            
            switch state {
                
            case kFsAudioStreamErrorOpen:
                errorCategory = "Cannot open the audio stream: ";
                break;
                
            case kFsAudioStreamErrorStreamParse:
                errorCategory = "Cannot read the audio stream: ";
                break;
                
            case kFsAudioStreamErrorNetwork:
                errorCategory = "Network failed: cannot play the audio stream: ";
                break;
                
            case kFsAudioStreamErrorUnsupportedFormat:
                errorCategory = "Unsupported format: ";
                break;
                
            case kFsAudioStreamErrorStreamBouncing:
                errorCategory = "Network failed: cannot get enough data to play: ";
                break;
                
            default:
                errorCategory = "Unknown error occurred: ";
                break;
            }
            
            print(errorCategory, description)
        }
    }
    
    func freeStreamer(freeStreamer: FSAudioController?, onMetaDataAvailable: [NSObject : AnyObject]?) {
        
        MetadataManager.metadataParser(self.currentStation, track: self.track, metadata: onMetaDataAvailable, process: { () -> Void in
            
            self.startNowPlayingAnimation()
            
            }) { (track, finished) -> Void in
                
                self.track = track
                
                if finished {
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.artistLabel.text = self.track.artist
                        self.songLabel.text = self.track.title
                        
                        self.songLabel.animation = "zoomIn"
                        self.songLabel.duration = 1.5
                        self.songLabel.damping = 1
                        self.songLabel.animate()
                    }
                    
                    self.delegate?.didSongMetaDataUpdate(self.track)
                    
                    MetadataManager.queryAlbumArt(self.track, process: { (type) -> Void in
                        
                        self.clearAlbumArtwork()
                        
                        let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder), "Action": "use cover grabber " + type.rawValue]
                        Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
                        
                        }, success: { (track, type, success) -> Void in
                            
                            self.track = track

                            if success {

                                self.updateAlbumArtwork(type)
                                
                            } else {
                                
                                self.clearAlbumArtwork()
                            }
                            
                            MetadataManager.updateLockScreen(self.track)
                    })
                    
                } else {
                    
                    self.clearAlbumArtwork()
                    MetadataManager.updateLockScreen(self.track)
                }
        }
    }
}
