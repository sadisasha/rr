//
//  SideMenuController.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 10.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit
import AVFoundation

class SideMenuController: UIViewController, UITableViewDelegate, UITableViewDataSource, NowStationPlayingControllerDelegate, FreeStreamerDelegate {

    @IBOutlet var backgroundImageView : UIImageView!
    @IBOutlet var tableView   : UITableView!
    
    @IBOutlet var dimmerView  : UIView!
    @IBOutlet var bottomView  : UIView!
    
    @IBOutlet weak var stationNowPlayingButton: UIButton!
    @IBOutlet weak var nowPlayingAnimationImageView: UIImageView!
    
    var items: Array<AnyObject> = Array()

    var currentTrack: Track?
    var currentStation: Station?
    
    private var downloadTask: NSURLSessionDownloadTask?
    
    private let radioPlayer = FreeStreamer.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didReopenStationAfterSuspend(_:)), name: kReopenStationAfterSuspendKey, object: nil)
        
        self.tableView.separatorStyle = .None
        self.tableView.backgroundColor = UIColor.clearColor()
        
        self.dimmerView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        
        self.backgroundImageView.image = UIImage(named: "sidemenu-bg")
        
        self.createNowPlayingAnimation()
        
        if NSUserDefaults.standardUserDefaults().boolForKey(kPlayingAfterAppLaunchKey) {
            
            if let dictionary = NSUserDefaults.standardUserDefaults().objectForKey(kCurrentStationKey) as? Dictionary<String, AnyObject> {
                
                self.currentStation = Station(dictionary: dictionary)
                
                let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let controller = storyboard.instantiateViewControllerWithIdentifier("NowStationPlayingController") as! NowStationPlayingController
                
                controller.delegate = self
                
                controller.currentStation = self.currentStation
                controller.newStation = true
                
                self.navigationController?.pushViewController(controller, animated: false)
            }
        
        } else {
            
            NSUserDefaults.standardUserDefaults().removeObjectForKey(kCurrentStationKey)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        FreeStreamer.sharedInstance.delegate = self
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder)]
        Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
        
        self.reloadContent()
        
        self.radioPlayer.prepareToPlay()
        
        if let station = self.currentStation, track = self.currentTrack where track.isPlaying {
            
            self.stationNowPlayingButton.enabled = true
            
            let title = "\(station.name!): \(track.title)..."
            self.stationNowPlayingButton.setTitle(title, forState: .Normal)
            self.nowPlayingAnimationImageView.startAnimating()
            
        } else {
            
            self.nowPlayingAnimationImageView.stopAnimating()
            self.nowPlayingAnimationImageView.image = UIImage(named: "NowPlayingBars")
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        FreeStreamer.sharedInstance.delegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {

        NSNotificationCenter.defaultCenter().removeObserver(self, name: kReopenStationAfterSuspendKey, object: nil)
    }
    
    func didReopenStationAfterSuspend(notification: NSNotification?) {
        
        if NSUserDefaults.standardUserDefaults().boolForKey(kPlayingAfterAppLaunchKey) {
            
            NSUserDefaults.standardUserDefaults().setObject(self.currentStation?.dictionary(), forKey: kCurrentStationKey)
            
        } else {
            
            NSUserDefaults.standardUserDefaults().removeObjectForKey(kCurrentStationKey)
        }
        
        if let track = self.currentTrack where notification?.object != nil {

            let bitrate = NSUserDefaults.standardUserDefaults().integerForKey(kSoundQualityBitrateKey)
            
            if bitrate == track.bitrate { return }

            let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder), "Action": "change bitrate with \(bitrate)"]
            Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
            
            self.currentTrack?.bitrate = bitrate
            
            var kbpsKey: String?
            
            if bitrate == 0 {
                
                kbpsKey = "32"
                
            } else if bitrate == 1 {
                
                kbpsKey = "128"
                
            } else {
                
                kbpsKey = "320"
            }

            if let key = kbpsKey, station = self.currentStation, url = station.urls?[key] {
            
                self.radioPlayer.stop()
                
                let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder), "Action": "open url \(url)"]
                Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
                
                RadioRecordLogger.Log("Play station with url: \(url)")
                
                self.radioPlayer.currentURL = url
                self.radioPlayer.prepareToPlay()
                self.radioPlayer.play()

                self.currentTrack?.isPlaying = true
            }
        }
    }
    
    func createNowPlayingAnimation() {
        
        self.nowPlayingAnimationImageView.animationImages = AnimationUtils.createFrames()
        self.nowPlayingAnimationImageView.animationDuration = 0.8
    }
    
    func reloadContent() {
        
        if let items = StationManager.sharedInstance.stationCollection?.array {
            
            self.items.removeAll()
            
            for item in items {
                
                if CoreDataManager.sharedInstance.containts(item.name) {
                    
                    self.items.append(item)
                }
            }
            
            self.items.append(NSLocalizedString("editStationListTitle", comment: "Edit station list title"))
            
            self.tableView.reloadData()
        }
    }
    
    //MARK: - @IBAction
    
    @IBAction func onOpenSettingsAction(sender: UIBarButtonItem?) {

        self.performSegueWithIdentifier("Settings", sender: self)
    }
    
    @IBAction func openPlayStation(sender: UIButton?) {

        if let track = self.currentTrack where !track.isPlaying {
            
            self.currentTrack?.isPlaying = true
        }
        
        self.performSegueWithIdentifier("ShowPlayer", sender: self)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StationCell") as! StationCell
        
        if let item = self.items[indexPath.row] as? Station {

            cell.stationImage.image = UIImage(named: "\(item.name!)-square")
            
            cell.stationTitle.font = RadioRecordTableViewCellTitleFont
            cell.stationTitle.textColor = UIColor.whiteColor()
            
            cell.stationTitle.text = item.name!
            
        } else if let item = self.items[indexPath.row] as? String {
            
            cell.stationImage.image = UIImage(named: "plus_circle")
            
            cell.stationTitle.font = RadioRecordTableViewCellOtherFont
            cell.stationTitle.textColor = UIColor.lightTextColor()
            
            cell.stationTitle.text = item
        }
        
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if indexPath.row == self.items.count - 1 {
         
            self.performSegueWithIdentifier("AddStations", sender: self)
            
        } else {

            self.performSegueWithIdentifier("ShowPlayer", sender: indexPath)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if let toViewController = segue.destinationViewController as? NowStationPlayingController {

            if let indexPath = sender as? NSIndexPath, station = self.items[indexPath.row] as? Station {
                
                toViewController.currentStation = station
                
                if self.currentStation?.name != station.name {
                    
                    toViewController.newStation = true
                    self.currentStation = station

                    if NSUserDefaults.standardUserDefaults().boolForKey(kPlayingAfterAppLaunchKey) {
                    
                        NSUserDefaults.standardUserDefaults().setObject(self.currentStation?.dictionary(), forKey: kCurrentStationKey)
                    
                    } else {
                        
                        NSUserDefaults.standardUserDefaults().removeObjectForKey(kCurrentStationKey)
                    }
                    
                } else {
                    
                    toViewController.track = currentTrack
                    toViewController.newStation = false
                }
                
            } else {
                
                if let currentTrack = self.currentTrack {
                    
                    toViewController.track = currentTrack
                    toViewController.currentStation = self.currentStation
                    toViewController.newStation = false
                    
                } else {
                    
                    toViewController.currentStation = self.currentStation
                    toViewController.newStation = true
                }
            }
            
            toViewController.delegate = self
        }
    }
    
    //MARK: - NowStationPlayingControllerDelegate
    
    func didArtworkUpdate(track: Track) {
        
        self.currentTrack?.artworkURL = track.artworkURL
        self.currentTrack?.artworkImage = track.artworkImage
    }
    
    func didSongMetaDataUpdate(track: Track) {
        
        self.currentTrack = track
        
        if let station = self.currentStation, track = self.currentTrack where track.isPlaying {
            
            self.stationNowPlayingButton.enabled = true
            
            let title = "\(station.name!): \(track.title)..."
            self.stationNowPlayingButton.setTitle(title, forState: .Normal)
            self.nowPlayingAnimationImageView.startAnimating()
            
        } else {
            
            self.stationNowPlayingButton.enabled = false
            
            self.nowPlayingAnimationImageView.stopAnimating()
            self.nowPlayingAnimationImageView.image = UIImage(named: "NowPlayingBars")
        }
    }
    
    func trackPlayingToggled(track: Track) {
        
        self.currentTrack?.isPlaying = track.isPlaying
        
        if !track.isPlaying { self.nowPlayingAnimationImageView.stopAnimating() } else { self.nowPlayingAnimationImageView.startAnimating() }
    }
    
    //MARK: - FreeStreamerDelegate methods
    
    func freeStreamer(freeStreamer: FSAudioController?, onStateChange: FSAudioStreamState?) {
        
        if let state = onStateChange {
            
            var changeCategory = "..."
            
            switch state {
                
            case kFsAudioStreamRetrievingURL:
                changeCategory = "Retrieving URL..."
                break
                
            case kFsAudioStreamStopped:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                changeCategory = "Stopped..."
                break
                
            case kFsAudioStreamPaused:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                changeCategory = "Stopped..."
                break
                
            case kFsAudioStreamBuffering:
                changeCategory = "Buffering..."
                break
                
            case kFsAudioStreamSeeking:
                changeCategory = "Seeking..."
                break
                
            case kFsAudioStreamPlaying:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                changeCategory = "Playing..."
                break
                
            case kFsAudioStreamFailed:
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                changeCategory = "Failed..."
                break
                
            default: break
            }
            
            print(changeCategory)
        }
    }
    
    func freeStreamer(freeStreamer: FSAudioController?, onFailure: FSAudioStreamError?, description: String?) {
        
        if let state = onFailure {
            
            var errorCategory = "..."
            
            switch state {
                
            case kFsAudioStreamErrorOpen:
                errorCategory = "Cannot open the audio stream: ";
                break;
                
            case kFsAudioStreamErrorStreamParse:
                errorCategory = "Cannot read the audio stream: ";
                break;
                
            case kFsAudioStreamErrorNetwork:
                errorCategory = "Network failed: cannot play the audio stream: ";
                break;
                
            case kFsAudioStreamErrorUnsupportedFormat:
                errorCategory = "Unsupported format: ";
                break;
                
            case kFsAudioStreamErrorStreamBouncing:
                errorCategory = "Network failed: cannot get enough data to play: ";
                break;
                
            default:
                errorCategory = "Unknown error occurred: ";
                break;
            }
            
            print(errorCategory, description)
        }
    }
    
    func freeStreamer(freeStreamer: FSAudioController?, onMetaDataAvailable: [NSObject : AnyObject]?) {
        
        MetadataManager.metadataParser(self.currentStation, track: self.currentTrack, metadata: onMetaDataAvailable, process: { () -> Void in
            
            self.nowPlayingAnimationImageView.startAnimating()
            
            }) { (track, finished) -> Void in
                
                self.currentTrack = track
                
                if finished {
                    
                    self.didSongMetaDataUpdate(track)
                    
                    MetadataManager.queryAlbumArt(track, process: { (type) -> Void in
                        
                        let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder), "Action": "use cover grabber " + type.rawValue]
                        Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)
                        
                        }, success: { (track, type, success) -> Void in
                            
                            self.currentTrack = track
                            
                            if success {
                                
                                if NSUserDefaults.standardUserDefaults().boolForKey(kSaveAlbumCoverKey) && type != .Local {
                                    
                                    ImageManager.save(track, type: type == .LastFm ? ExtType.PNG : ExtType.JPG)
                                }
                            }
                            
                            self.didArtworkUpdate(track)
                            self.didSongMetaDataUpdate(track)
                            
                            MetadataManager.updateLockScreen(track)
                    })
                    
                } else {
                    
                    self.didArtworkUpdate(track)
                    self.didSongMetaDataUpdate(track)
                    
                    MetadataManager.updateLockScreen(track)
                }
        }
    }
}
