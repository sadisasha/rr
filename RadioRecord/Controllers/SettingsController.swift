//
//  SettingsController.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 17.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit
import Bohr

class SettingsController: BOTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.Default, animated: false)
        
        let dicitionary: Dictionary<NSObject, AnyObject> = ["ControllerName": NSStringFromClass(self.classForCoder)]
        Flurry.logEvent("Opened", withParameters: dicitionary, timed: true)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("doneButtonTitle", comment: "Done button title"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onCloseAction(_:)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Setup
    
    override func setup() {

        self.title = NSLocalizedString("settingsNavigationBarTitle", comment: "Settings NavigationBar title")
        
        self.addSection(BOTableViewSection(headerTitle: nil, handler: { (section) -> Void in
            
            section.addCell(BOSwitchTableViewCell(title: NSLocalizedString("playAfterLaunchTitle", comment: "Play after launch title"), key: kPlayingAfterAppLaunchKey, handler: nil))
        
//            section.addCell(BOChoiceTableViewCell(title: NSLocalizedString("serviceRecognitionMetadataTitle", comment: "Service recognition metadata title"), key: kChooseReceiveInformationTrackKey, handler: { (cell) -> Void in
//                
//                if let cell = cell as? BOChoiceTableViewCell {
//
//                    cell.destinationViewController = MetadataParserController()
//                }
//            }))
            
            section.addCell(BOChoiceTableViewCell(title: NSLocalizedString("soundQualityTitle", comment: "Sound quality title"), key: kSoundQualityBitrateKey, handler: { (cell) -> Void in
                
                if let cell = cell as? BOChoiceTableViewCell {

                    cell.destinationViewController = SoundQualityController()
                }
            }))
             
            section.addCell(BOSwitchTableViewCell(title: NSLocalizedString("saveAlbumCoverTitle", comment: "Save th album cover title"), key: kSaveAlbumCoverKey, handler: { (cell) -> Void in
            
                if let cell = cell as? BOSwitchTableViewCell {
                
                    cell.toggleSwitch.addTarget(self, action: #selector(self.saveAlbumCoverValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
                }
            }))
            
            section.addCell(BOButtonTableViewCell(title: NSLocalizedString("defaultStationListTitle", comment: "Default station list title"), key: nil, handler: { (cell) -> Void in
                
                if let cell = cell as? BOButtonTableViewCell {
                    
                    cell.actionBlock = {

                        if StationManager.sharedInstance.resetStationList() {
                            
                            let subTitle = NSLocalizedString("listStationsRestoredTitle", comment: "List stations restored title")
                            SCLAlertView().showTitle("RadioRecord", subTitle: subTitle, duration: 5.0, completeText: NSLocalizedString("successButtonTitle", comment: "Success button title"), style: SCLAlertViewStyle.Success, colorStyle: 0x22B573, colorTextButton: 0xFFFFFF)

                        } else {
                            
                            let subTitle = NSLocalizedString("failedRetrieveListStationsTitle", comment: "Failed retrieve list stations title")
                            SCLAlertView().showTitle("RadioRecord", subTitle: subTitle, duration: 5.0, completeText: NSLocalizedString("errorButtonTitle", comment: "Error button title"), style: SCLAlertViewStyle.Error, colorStyle: 0xC1272D, colorTextButton: 0xFFFFFF)
                        }
                    }
                }
            }))
        }))
    }
    
    //MARK: - Actions
    
    func saveAlbumCoverValueChanged(sender: UISwitch?) {
        
        if let value = sender?.on where !value {
            
            let alertView = SCLAlertView()
            alertView.addButton(NSLocalizedString("turnOffWithRemoveAllImagesTitle", comment: "TurnOff with remove all images title"), action: { () -> Void in
             
                ImageManager.removeAll()
            })
            alertView.addButton(NSLocalizedString("turnOffWithoutRemovingTitle", comment: "TurnOff without removing title"), action: { () -> Void in
                
            })
            alertView.addButton(NSLocalizedString("cancelButtonTitle", comment: "Cancel button title"), action: { () -> Void in
             
                sender?.setOn(true, animated: true)
            })
            alertView.showCloseButton = false
            alertView.showTitle("RadioRecord", subTitle: NSLocalizedString("disableFeatureAndDeleteAllImagesTitle", comment: "Disable this feature and delete all images title"), duration: 0, completeText: nil, style: SCLAlertViewStyle.Info, colorStyle: 0x2866BF, colorTextButton: 0xFFFFFF)
        }
    }
    
    func onCloseAction(sender: UIBarButtonItem?) {

        self.dismissViewControllerAnimated(true) { () -> Void in
            
            let stationName = FreeStreamer.sharedInstance.currentStationName != nil ? FreeStreamer.sharedInstance.currentStationName : ""
            
            if let URLEncodedText = stationName?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
                
                let path = "radiorecord://".stringByAppendingString(URLEncodedText)
                if let URL = NSURL(string: path) where UIApplication.sharedApplication().canOpenURL(URL) {
                    
                    UIApplication.sharedApplication().openURL(URL)
                }
            }
        }
    }
}

class MetadataParserController: BOTableViewController {
    
    override func setup() {
        
        //self.title = NSLocalizedString("choiceServiceTitle", comment: "Choice service title")
        
        self.addSection(BOTableViewSection(headerTitle: nil, handler: { (section) -> Void in
            
            section.addCell(BOOptionTableViewCell(title: "iTunes", key: kChooseReceiveInformationTrackKey, handler: { (cell) -> Void in
                
//                if let cell = cell as? BOOptionTableViewCell {
//                    
//                    //cell.setFooterTitle("wwwwwwww")
//                }
            }))
            
            section.addCell(BOOptionTableViewCell(title: "LastFm", key: kChooseReceiveInformationTrackKey, handler: { (cell) -> Void in
                
//                if let cell = cell as? BOOptionTableViewCell {
//                    
//                    //cell.setFooterTitle("ddddddd")
//                }
            }))
        }))
    }
}

class SoundQualityController: BOTableViewController {
    
    override func setup() {
        
        //self.title = NSLocalizedString("soundQualityTitle", comment: "Sound quality title")
        
        self.addSection(BOTableViewSection(headerTitle: nil, handler: { (section) -> Void in
            
            section.addCell(BOOptionTableViewCell(title: "32 kbps", key: kSoundQualityBitrateKey, handler: { (cell) -> Void in

//                if let cell = cell as? BOOptionTableViewCell {
//                    
//                    //cell.setFooterTitle("wwwwwwww")
//                }
            }))
            
            section.addCell(BOOptionTableViewCell(title: "128 kbps", key: kSoundQualityBitrateKey, handler: { (cell) -> Void in

//                if let cell = cell as? BOOptionTableViewCell {
//                    
//                    //cell.setFooterTitle("ddddddd")
//                }
            }))
            
            section.addCell(BOOptionTableViewCell(title: "320 kbps", key: kSoundQualityBitrateKey, handler: { (cell) -> Void in

//                if let cell = cell as? BOOptionTableViewCell {
//                    
//                    //cell.setFooterTitle("ddddddd")
//                }
            }))
        }))
    }
}