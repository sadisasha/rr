// Generated by xsd compiler for ios/objective-c
// DO NOT CHANGE!

#import <Foundation/Foundation.h>
#import "PicoClassSchema.h"
#import "PicoPropertySchema.h"
#import "PicoConstants.h"
#import "PicoBindable.h"


@class NewRelease;

/**
 (public class)
 
 @ingroup AWSECommerceServicePortType
*/
@interface NewReleases : NSObject <PicoBindable> {

@private
    NSMutableArray *_newRelease;

}

#ifndef __has_attribute
#define __has_attribute(x) 0  // Compatibility with non-clang compilers
#endif

#if __has_attribute(objc_method_family)
#define BV_OBJC_METHOD_FAMILY_NONE __attribute__((objc_method_family(none)))
#else
#define BV_OBJC_METHOD_FAMILY_NONE
#endif

/**
 (public property)
 
 entry type : class NewRelease
*/

@property (nonatomic, retain, getter=theNewRelease) NSMutableArray *newRelease;


@end
