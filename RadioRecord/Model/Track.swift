//
//  Track.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 14.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer

class Track: NSObject {
    
    var bitrate = 1
    var title: String = ""
    var artist: String = ""
    var artworkURL: String = ""
    var artworkImage: UIImage? = UIImage()
    var artworkLoaded = false
    var isPlaying: Bool = false
}