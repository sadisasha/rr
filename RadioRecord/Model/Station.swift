//
//  Station.swift
//  Record
//
//  Created by Aleksandr Sadikov on 09.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import UIKit

class Station: NSObject {

    var name: String?
    var desc: String?
    var urls: Dictionary<String, NSURL>?
    var checked: Bool?
    
    init(name: String?, urls: Dictionary<String, NSURL>?) {
        super.init()
        
        self.name = name
        self.urls = urls
    }
    
    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        
        self.name = dictionary["name"] as? String
        self.desc = dictionary["desc"] as? String
        if let urlsDictionary = dictionary["urls"] as? Dictionary<String, AnyObject> {
            self.urls = Dictionary<String, NSURL>()
            for key in urlsDictionary.keys {
                if let value = urlsDictionary[key] as? String, url = NSURL(string: value) {
                    self.urls?.updateValue(url, forKey: key)
                }
            }
        }
        self.checked = dictionary["checked"] as? Bool
    }
    
    func dictionary() -> Dictionary<String, AnyObject> {
        
        var dictionary = Dictionary<String, AnyObject>()
        
        if let name = self.name {
            
            dictionary.updateValue(name, forKey: "name")
        }

        if let desc = self.desc {
            
            dictionary.updateValue(desc, forKey: "desc")
        }

        if let urls = self.urls {
            
            var new = Dictionary<String, AnyObject>()
            for key in urls.keys {
                
                if let value = urls[key]?.absoluteString {
                 
                    new.updateValue(value, forKey: key)
                }
            }
            
            dictionary.updateValue(new, forKey: "urls")
        }

        if let checked = self.checked {
            
            dictionary.updateValue(checked, forKey: "checked")
        
        } else {
            
            dictionary.updateValue(true, forKey: "checked")
        }

        return dictionary
    }
}

class StationCollection: NSObject {
    
    var array: Array<Station>?
    
    override init() {

        self.array = Array()
    }

    init(array: Array<AnyObject>?) {
        
        self.array = Array()
        
        if let objects = array {
            
            for object in objects {
                
                if let item = object as? Station {
                    
                    self.array?.append(item)
                }
            }
        }
    }
    
    func append(item: Station) {
        
        self.array?.append(item)
    }
    
    var itemCount: Int {
        
        get {
        
            if let array = self.array {
                
                return array.count
            }
            
            return 0
        }
    }
}
