//
//  RadioRecord-Settings.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 16.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation
import UIKit

//**************************************
// GENERAL SETTINGS
//**************************************

let kReopenStationAfterSuspendKey = "ReopenStationAfterSuspendKey"
let kPlayingAfterAppLaunchKey = "PlayingAfterAppLaunch"
let kChooseReceiveInformationTrackKey = "ChooseReceiveInformationTrack"
let kSoundQualityBitrateKey = "SoundQualityBitrate"
let kSaveAlbumCoverKey = "SaveAlbumCover"
let kPlayerVolumeKey = "PlayerVolume"

let kCurrentStationKey = "CurrentStation"

let RadioRecordNavigationBarTitleFont = UIFont(name: "AvenirNext-Bold", size: 16)
let RadioRecordToastMessageFont = UIFont(name: "AvenirNext-Regular", size: 16)
let RadioRecordToastTitleFont = UIFont(name: "Avenir-Black", size: 12)
let RadioRecordTableViewCellTitleFont = UIFont.init(name: "Avenir-Black", size: 16)
let RadioRecordTableViewCellOtherFont = UIFont.init(name: "Avenir-Black", size: 12)

// Display Comments
#if DEBUG
    let DEBUG_LOG = true
#else
    let DEBUG_LOG = false
#endif

//**************************************
// LASTFM API
//**************************************

let LAST_FM_API_KEY    = "1c765f79b57b4cafe65c823d31f8100e"
let LAST_FM_API_SECRET = "bcd4c84ce5b6568b07e9988a62601c7a"

//**************************************
// Flurry API
//**************************************

let FLURRY_API_KEY = "7W89T4Z63JCVXKC3JHBC"