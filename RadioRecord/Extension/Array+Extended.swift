//
//  Array+Extended.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 24.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation

extension Array {
    
    mutating func replace(element: Element, atIndex: Int) {
        
        self.removeAtIndex(atIndex)
        self.insert(element, atIndex: atIndex)
    }
}