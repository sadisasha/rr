//
//  UIView+BlurEffect.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 11.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func backgroundBlurEffect() {
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            
            self.backgroundColor = UIColor.clearColor()
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            
            let vibrancyEffect = UIVibrancyEffect(forBlurEffect: blurEffect)
            let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
            
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            
            vibrancyView.frame = self.bounds
            vibrancyView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            
            blurEffectView.contentView.addSubview(vibrancyView)
            
            self.addSubview(blurEffectView)
            
        } else {
            
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        }
    }
}