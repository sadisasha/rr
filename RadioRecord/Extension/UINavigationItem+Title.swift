//
//  UINavigationItem+Title.swift
//  RadioRecord
//
//  Created by Aleksandr Sadikov on 24.01.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationItem {
    
    func title(title: String?) {
        
        if let title = title {
            
            let font = RadioRecordNavigationBarTitleFont
            
            let size = (title as NSString).sizeWithAttributes([NSFontAttributeName: font!])
            let frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            
            let titleLabel = UILabel(frame: frame)
            
            titleLabel.text = title
            titleLabel.textColor = UIColor.lightTextColor()
            titleLabel.font = font
            
            self.titleView = titleLabel
        }
    }
}